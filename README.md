# Project iot-watch

This is a small project for demonstrate listening things event via websocket. 

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

* Install JDK 8
* Install sbt
* Install Redis(Option, if you want try send event by publish message to Redis)

### Running

After pull down the project from gitlab. you can use sbt to run the project.
This project can run with two mode. 
* Fake Mode: fake event generator which will generate fake thing event every 1 second. 
* Redis Mode: will connect to Redis to subscribe thing event.

#### Running in Fake Mode
 in project directory, run command
```
sbt run
```
#### Running in Redis Mode
```
sbt run -Dcom.wolfwolfgod.mode=redis -Dcom.wolfwolfgod.redis=192.168.1.111
```
note: 192.168.1.111 is the redis server address. you should replace it with your address.

#### UI
visit http://localhost:9000/  , this will show a page for building websocket connection to listening event. Device Id is fixed(7784733de), you can chose which property you want to watch and the condition you want to receive notify. when you click listen button, it will send json command to server through websocket.

for example:
watch status when it is open
```
{"deviceId":"7784733de","command":"watch","property":"status","operation":"EQ","targetValue":"open"}
```
or watch temperature when it is greater then 3  
```
{"deviceId":"7784733de","command":"watch","property":"temperature","operation":"GT","targetValue":"3"}
```

after listening, it will show received message on the page. also you can  unlisten it by click the button, which will send command json
```
{"deviceId":"7784733de","command":"unwatch","property":"status"}
```

#### Backend Event Data
As described earlier, we have a fake event generator which will generate random even(status:open/close, temperature: 0-100) every second. But if you use Redis. you should publish message to channel(7784733de) manually. you can do this use redis-cli
```
127.0.0.1:6379> publish 7784733de "{\"deviceId\":\"7784733de\",\"property\":\"status\",\"value\":\"open\"}"
```

## Running the tests

To run the test, you can use sbt
```
 sbt test
```
note: test only work on fake mode. check  the listen and unlisten action via websocket. Test case is in FunctionalTest.java

## Build Dist
use sbt command to build dist
```
sbt dist
```
it will generate archive zip in target\universal directory,  the archive include .bat and bash file than can execute. 

## Architecture
we can start many play instances behind a Load Balance. All play install use akka to implement a reactive system. and they all connect to Redis, subscribe channel which be identified by device id. I chose Redis because it is simple to start up using docker. We can replace it with RabbitMQ or Kafka. They can provide more features for cluster and high availability.
![architecture](http://on-img.com/chart_image/5b04fb4ce4b0da6de338f65b.png?_=1527056617770)

## Authors

* **Ruimin Chen**


