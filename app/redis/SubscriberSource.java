package redis;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import akka.event.Logging;
import akka.event.LoggingAdapter;
import akka.japi.Option;
import akka.stream.Attributes;
import akka.stream.Outlet;
import akka.stream.Shape;
import akka.stream.SourceShape;
import akka.stream.stage.GraphStage;
import akka.stream.stage.GraphStageLogic;
import akka.stream.stage.OutHandler;

public class SubscriberSource extends GraphStage<SourceShape<Message>> {
    private final Logger logger = org.slf4j.LoggerFactory.getLogger(this.getClass());

	private final Channel channel;

	public SubscriberSource(String channelName) {
		this.channel = new Channel(channelName);
	}

	private Outlet<Message> out = new Outlet("SubscriberSource.Out");

	@Override
	public SourceShape<Message> shape() {
		return new SourceShape(out);
	}

	@Override
	public GraphStageLogic createLogic(Attributes inheritedAttributes) throws Exception {
		GraphStageLogic logic = new CustomGraphStageLogic(shape());
		return logic;
	}

	class CustomGraphStageLogic extends GraphStageLogic {
		private final CyclicBarrier barrier = new CyclicBarrier(2);
		private Option<Message> message = Option.none();
		private Option<String> threadName = Option.none();

		public CustomGraphStageLogic(Shape shape) {
			super(shape);
			setHandler(out, new OutHandler() {
				@Override
				public void onPull() throws Exception {
					barrier.await();
					logger.debug("subscribed message: {}", message.get());
					push(out, message.get());
					message = Option.none();
					barrier.reset();
				}
			});

		}

		private final RedisClient.MessageReceiver messageReceiver = new RedisClient.MessageReceiver() {

			@Override
			public void handleMessage(Message m) {
				message = Option.option(new Message(m.getContext()));
				try {
					barrier.await();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (BrokenBarrierException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		};

		@Override
		public void postStop() throws Exception {
			super.postStop();
			RedisClient.unsubscribe(threadName.get());
		}

		@Override
		public void preStart() throws Exception {
			super.preStart();
			threadName = Option.option(RedisClient.subscribe(channel, messageReceiver));
		}

	}
}
