package redis;

public class Message {
	private final String context;

	public String getContext() {
		return context;
	}

	public Message(String context) {
		super();
		this.context = context;
	};
	
}
