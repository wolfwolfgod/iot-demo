package redis;

public class Channel {
	private final String name;

	public Channel(String name) {
		super();
		this.name = name;
	}

	public String getName() {
		return name;
	}

}
