package redis;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;

import akka.japi.tuple.Tuple3;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPubSub;

public class RedisClient {
    private static final Logger logger = org.slf4j.LoggerFactory.getLogger(RedisClient.class);
	private static String host = "localhost";

	private static Map<String, Tuple3<Thread, Jedis, JedisPubSub>> subscribers = new HashMap<String, Tuple3<Thread, Jedis, JedisPubSub>>();

	public static String subscribe(Channel channel, MessageReceiver messageReceiver) {
		if(StringUtils.isNoneBlank(System.getProperty("com.wolfwolfgod.redis"))) {
			host=System.getProperty("com.wolfwolfgod.redis");
		}
		Jedis jedis = new Jedis(host);
		JedisPubSub pubsub = createPubSub(messageReceiver);
		Thread thread = new Thread(new Runnable() {
			@Override
			public void run() {
				logger.debug("start subscribe to {}",channel.getName());
				jedis.subscribe(pubsub, channel.getName());
			}
		});
		subscribers.put(thread.getName(), Tuple3.apply(thread, jedis, pubsub));

		thread.start();
		return thread.getName();
	}

	public static void unsubscribe(String name) {
		Tuple3<Thread, Jedis, JedisPubSub> tuple = subscribers.get(name);
		tuple.t3().unsubscribe();
		tuple.t2().quit();
	}

	private static JedisPubSub createPubSub(MessageReceiver messageReceiver) {
		return new JedisPubSub() {

			@Override
			public void onMessage(String channel, String message) {
				messageReceiver.handleMessage(new Message(message));
				logger.debug("onMessage");
			}

			@Override
			public void onPMessage(String pattern, String channel, String message) {
				super.onPMessage(pattern, channel, message);
				logger.debug("onPMessage");
			}

			@Override
			public void onSubscribe(String channel, int subscribedChannels) {
				super.onSubscribe(channel, subscribedChannels);
				logger.debug("onSubscribe");
			}

			@Override
			public void onUnsubscribe(String channel, int subscribedChannels) {
				super.onUnsubscribe(channel, subscribedChannels);
				logger.debug("onUnsubscribe");
			}

			@Override
			public void onPUnsubscribe(String pattern, int subscribedChannels) {
				super.onPUnsubscribe(pattern, subscribedChannels);
				logger.debug("onPUnsubscribe");
			}

			@Override
			public void onPSubscribe(String pattern, int subscribedChannels) {
				super.onPSubscribe(pattern, subscribedChannels);
				logger.debug("onPSubscribe");
			}

		};
	}

	public static interface MessageReceiver {
		public void handleMessage(Message m);
	}
}
