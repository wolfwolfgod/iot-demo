package actors;

import java.util.Set;

import akka.japi.Pair;
import events.Thing;
import static java.util.Objects.requireNonNull;

public final class Message {
	//operation support greater than, less than, Equal to, Not Equal to
	public enum Operation {  
		  GT, LT, EQ, NE  
	}  
	
	public static final class WatchTarget {		
		final String deviceId;
		final String property;
		final Operation operation;  
		final String targetValue;
		
		public WatchTarget(String deviceId, String property, Operation operation, String targetValue) {
			this.deviceId = deviceId;
			this.property = property;
			this.operation = operation;
			this.targetValue = targetValue;
		}

		public WatchTarget(String deviceId, String property, String operation, String targetValue) {
			this.deviceId = deviceId;
			this.property = property;
			this.operation = Operation.valueOf(operation);
			this.targetValue = targetValue;
		}

		
        @Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((deviceId == null) ? 0 : deviceId.hashCode());
			result = prime * result + ((operation == null) ? 0 : operation.hashCode());
			result = prime * result + ((property == null) ? 0 : property.hashCode());
			result = prime * result + ((targetValue == null) ? 0 : targetValue.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			WatchTarget other = (WatchTarget) obj;
			if (deviceId == null) {
				if (other.deviceId != null)
					return false;
			} else if (!deviceId.equals(other.deviceId))
				return false;
			if (operation != other.operation)
				return false;
			if (property == null) {
				if (other.property != null)
					return false;
			} else if (!property.equals(other.property))
				return false;
			if (targetValue == null) {
				if (other.targetValue != null)
					return false;
			} else if (!targetValue.equals(other.targetValue))
				return false;
			return true;
		}

		@Override
        public String toString() {
            return "WatchTarget(deviceId:" + deviceId.toString() + ", property:"+property+")";
        }
        
        
	}
	    
	public static final class UnWatchTarget {		
		final String deviceId;
		final String property;
		
		public UnWatchTarget(String deviceId, String property) {
			this.deviceId = deviceId;
			this.property = property;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((deviceId == null) ? 0 : deviceId.hashCode());
			result = prime * result + ((property == null) ? 0 : property.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			UnWatchTarget other = (UnWatchTarget) obj;
			if (deviceId == null) {
				if (other.deviceId != null)
					return false;
			} else if (!deviceId.equals(other.deviceId))
				return false;
			if (property == null) {
				if (other.property != null)
					return false;
			} else if (!property.equals(other.property))
				return false;
			return true;
		}
		
	}    
    
}
