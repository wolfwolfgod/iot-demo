package actors;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import com.fasterxml.jackson.databind.JsonNode;
import com.google.inject.assistedinject.Assisted;

import actors.Message.UnWatchTarget;
import actors.Message.WatchTarget;
import akka.Done;
import akka.NotUsed;
import akka.actor.AbstractActor;
import akka.actor.Actor;
import akka.actor.Status;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import akka.japi.Pair;
import akka.japi.function.Predicate;
import akka.stream.KillSwitches;
import akka.stream.Materializer;
import akka.stream.UniqueKillSwitch;
import akka.stream.javadsl.BroadcastHub;
import akka.stream.javadsl.Flow;
import akka.stream.javadsl.Keep;
import akka.stream.javadsl.MergeHub;
import akka.stream.javadsl.RunnableGraph;
import akka.stream.javadsl.Sink;
import akka.stream.javadsl.Source;
import akka.util.Timeout;
import events.Thing;
import play.libs.Json;
import scala.concurrent.duration.Duration;

public class ClientActor extends AbstractActor {

    private final Timeout timeout = new Timeout(Duration.create(5, TimeUnit.SECONDS));
    
    private final Materializer mat;
    
    private final String clientId;
    private final Map<String, UniqueKillSwitch> thingsMap = new HashMap<>();
    private final Map<String, Set<WatchTarget>> listenPropertiesMap = new HashMap<>();
    private final Sink<JsonNode, NotUsed> hubSink;
    private final Flow<JsonNode, JsonNode, NotUsed> websocketFlow;
    private final LoggingAdapter loger = Logging.getLogger(getContext().system(), this);
    
	public static class BuildSocket{
		
		public BuildSocket() {
		}
	}
	
	
	@Inject
	public ClientActor(@Assisted String clientId,  Materializer mat) {
		this.clientId = clientId;
		this.mat = mat;
		
		Pair<Sink<JsonNode, NotUsed>, Source<JsonNode, NotUsed>> sinkSourcePair = 
				MergeHub.of(JsonNode.class, 16).toMat(BroadcastHub.of(JsonNode.class,256), Keep.both())
				.run(mat);
		this.hubSink = sinkSourcePair.first();
		Source<JsonNode, NotUsed> hubSource = sinkSourcePair.second();
		
		Sink<JsonNode, CompletionStage<Done>> jsonSink = Sink.foreach((JsonNode json) -> {
            String command = json.findPath("command").asText();
            String deviceId = json.findPath("deviceId").asText();
            String property = json.findPath("property").asText();
            if(command.equals("watch")) {
            	//start to add watch on property of device
                String operation = json.findPath("operation").asText();
                String targetValue = json.findPath("targetValue").asText();
                final WatchTarget watchTarget = new WatchTarget(deviceId, property, operation, targetValue);
                loger.info("Adding listening on device {} ", deviceId);
                addWatch(watchTarget);            	
            }else if(command.equals("unwatch")) {
            	//remove watch on device property
                final UnWatchTarget unwatchTarget = new UnWatchTarget(deviceId, property);
                loger.info("remove listening on device {0} property {} ", deviceId, property);
                removeWatch(unwatchTarget);
            	
            }
        });
		
		this.websocketFlow = Flow.fromSinkAndSource(jsonSink, hubSource).watchTermination((n, stage) -> {
            // When the flow shuts down, make sure this actor also stops.
            stage.thenAccept(f -> context().stop(self()));
            return NotUsed.getInstance();
        });
		
	}

	@Override
	public Receive createReceive() {
		return receiveBuilder().match(BuildSocket.class, x -> {
			sender().tell(websocketFlow, self());
		}).match(UnWatchTarget.class, unWatchTarget -> {
			removeWatch(unWatchTarget);
		}).matchAny(x -> sender().tell(new Status.Failure(new Exception("unknown message")), self())).build();
	}

	/**
	 * remove device property from watch list, if no perperties were been watched, 
	 * then remove the device watch
	 * @param unWatchTarget
	 */
    private void removeWatch(UnWatchTarget unWatchTarget) {
    	if(listenPropertiesMap.containsKey(unWatchTarget.deviceId)) {
    		//remove from interesting properties list.
    		listenPropertiesMap.get(unWatchTarget.deviceId).removeIf(x->x.property.equals(unWatchTarget.property));
    	}
    	if(thingsMap.containsKey(unWatchTarget.deviceId) && listenPropertiesMap.containsKey(unWatchTarget.deviceId) && listenPropertiesMap.get(unWatchTarget.deviceId).size()==0) {
    		//stop watch by shutdown RunnableGraph
			thingsMap.get(unWatchTarget.deviceId).shutdown();
			thingsMap.remove(unWatchTarget.deviceId);
    	}
		
	}

	private void addWatch(WatchTarget watchTarget) {
		
		if(listenPropertiesMap.containsKey(watchTarget.deviceId)) {
			// add interesting property to watch list
			listenPropertiesMap.get(watchTarget.deviceId).add(watchTarget);
		}else {
			Set<WatchTarget> listenActions = new HashSet<WatchTarget>();
			listenActions.add(watchTarget);
			listenPropertiesMap.put(watchTarget.deviceId, listenActions);
		}		
		final Thing thing =  new Thing(watchTarget.deviceId);
		if (!thingsMap.containsKey(thing.deviceId)) {
			//add watch device
			addThing(thing);
		}
	}
	
	private void addThing(Thing thing) {
		loger.info("Adding Thing {}", thing);
		final Predicate<JsonNode> predicate = new Predicate<JsonNode>(){

			@Override
			public boolean test(JsonNode json) {
				String deviceId = json.findPath("deviceId").asText();
	            String property = json.findPath("property").asText();
	            String value = json.findPath("value").asText();
	            return isInteresting(deviceId, property, value);
			}
		};
		
		//convert to JSON and do filter unwatched properties
		final Source<JsonNode, NotUsed> source = thing.update().map(Json::toJson)
				.filter(predicate);
		
        // Set up a flow and pull out a killswitch .
		final Flow<JsonNode, JsonNode, UniqueKillSwitch> killswitchFlow = Flow.of(JsonNode.class)
                .joinMat(KillSwitches.singleBidi(), Keep.right());
		
		// Set up a complete runnable graph
		String name = "thing with id: "+thing.deviceId+" by client "+ clientId;
		final RunnableGraph<UniqueKillSwitch> graph = source.viaMat(killswitchFlow, Keep.right())
				.to(this.hubSink)
				.named(name);
		
		//start to run
		UniqueKillSwitch killSwitch = graph.run(mat);
		
		//put it in a map so we can stop it later.
		thingsMap.put(thing.deviceId, killSwitch);
		
	}
	
	/**
	 * check whether the property is in watch list or not
	 * @param deviceId
	 * @param property
	 * @param value
	 * @return
	 */
	protected boolean isInteresting(String deviceId, String property, String value) {		
		if(!listenPropertiesMap.containsKey(deviceId)) {
			return false;
		}
		return listenPropertiesMap.get(deviceId).stream().anyMatch(x->match(x,property, value));
		
	}

	/**
	 * compare the actual value and watch condition 
	 * @param x
	 * @param supplyProperty
	 * @param supplyValue
	 * @return
	 */
	private boolean match(WatchTarget x, String supplyProperty, String supplyValue) {
		if(!x.property.equals(supplyProperty)) {
			return false;
		}
		
		switch (x.operation){
			case GT:
				return supplyValue.compareTo(x.targetValue)>0;
			case LT:
				return supplyValue.compareTo(x.targetValue)<0;
			case EQ:
				return supplyValue.equals(x.targetValue);
			case NE:
				return !supplyValue.equals(x.targetValue);
			default:
				return false;
					
		}
	}

	public interface Factory {
        Actor create(String clientId);
    }

}
