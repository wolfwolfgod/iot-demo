package actors;

import static akka.pattern.PatternsCS.ask;
import static akka.pattern.PatternsCS.pipe;

import java.util.concurrent.CompletionStage;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import com.typesafe.config.Config;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.util.Timeout;
import play.libs.akka.InjectedActorSupport;
import scala.concurrent.duration.Duration;

public class ClientParentActor extends AbstractActor implements InjectedActorSupport {

	private final ClientActor.Factory childFactory;
	private final Timeout t = new Timeout(Duration.create(5, TimeUnit.SECONDS));
	
	public static class Create{
		final String clientId;
		
		public Create(String clientId) {
			this.clientId = clientId;
		}
	}
	
	@Inject
	public ClientParentActor(ClientActor.Factory childFactory, Config config) {
		this.childFactory = childFactory;
	}
	
	@Override
	public Receive createReceive() {
		return receiveBuilder()
				.match(Create.class, create->{
					ActorRef child = injectedChild(()-> childFactory.create(create.clientId),"clientActor"+create.clientId);
					
					CompletionStage<Object> future = ask(child, new ClientActor.BuildSocket(), t);
					pipe(future, context().dispatcher()).to(sender());
				})
				.build();
	}

}
