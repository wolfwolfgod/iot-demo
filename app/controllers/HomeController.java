package controllers;

import java.util.concurrent.CompletionStage;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import org.slf4j.Logger;

import com.fasterxml.jackson.databind.JsonNode;

import actors.ClientParentActor;
import akka.NotUsed;
import akka.actor.ActorRef;
import akka.stream.javadsl.Flow;
import akka.util.Timeout;
import play.libs.F.Either;
import play.mvc.Controller;
import play.mvc.Http.RequestHeader;
import play.mvc.Result;
import play.mvc.Results;
import play.mvc.WebSocket;
import scala.concurrent.duration.Duration;
import static akka.pattern.PatternsCS.ask;

/**
 * This controller return index page, on which can setting up a WebSocket, watching event listening or remove listening
 */
@Singleton
public class HomeController extends Controller {

	private final ActorRef clientParentActor;
	private final Timeout t = new Timeout(Duration.create(1, TimeUnit.SECONDS));
    private final Logger logger = org.slf4j.LoggerFactory.getLogger(this.getClass());
	
	@Inject
    public HomeController(@Named("clientParentActor") ActorRef clientParentActor) {
    	this.clientParentActor = clientParentActor;
	}

	/**
     * An action that renders an HTML page with a welcome message.
     * The configuration in the <code>routes</code> file means that
     * this method will be called when the application receives a
     * <code>GET</code> request with a path of <code>/</code>.
     */
    public Result index() {
        return ok(views.html.index.render());
    }
    
    public WebSocket ws() {
    	return WebSocket.Json.acceptOrResult(request->{
    		final CompletionStage<Flow<JsonNode, JsonNode, NotUsed>> future = wsFutureFlow(request);
    		final CompletionStage<Either<Result, Flow<JsonNode, JsonNode, ?>>> stage = future.thenApply(Either::Right);
    		return stage.exceptionally(this::logException);
    	});
    	
    }

	private CompletionStage<Flow<JsonNode, JsonNode, NotUsed>> wsFutureFlow(RequestHeader request) {
		long id = request.asScala().id();
		ClientParentActor.Create create = new ClientParentActor.Create(String.valueOf(id));
		
		return ask(clientParentActor, create, t).thenApply((Object flow) ->{
			final Flow<JsonNode, JsonNode, NotUsed> f = (Flow<JsonNode, JsonNode, NotUsed>) flow;
			return f.named("websocket");
		});
		
	}
	
    private Either<Result, Flow<JsonNode, JsonNode, ?>> logException(Throwable throwable) {
        logger.error("Cannot create websocket", throwable);
        Result result = Results.internalServerError("error");
        return Either.Left(result);
    }


}
