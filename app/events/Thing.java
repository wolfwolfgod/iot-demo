package events;

import static java.util.Objects.requireNonNull;

import java.util.Optional;
import java.util.concurrent.TimeUnit;

import com.fasterxml.jackson.databind.JsonNode;

import akka.NotUsed;
import akka.japi.Pair;
import akka.japi.function.Function;
import akka.stream.ThrottleMode;
import akka.stream.javadsl.Source;
import play.libs.Json;
import redis.Message;
import redis.SubscriberSource;
import scala.concurrent.duration.Duration;
import scala.concurrent.duration.FiniteDuration;

public class Thing {
	public final String deviceId;

	private final Source<ThingEvent, NotUsed> source;
	private final ThingEventGenerator thingEventGenerator;
	private static final FiniteDuration duration = Duration.create(1, TimeUnit.SECONDS);
	
	public Thing(String deviceId) {
		this.deviceId = requireNonNull(deviceId);
		thingEventGenerator = new FakeThingEventGenerator(deviceId);
		String mode = System.getProperty("com.wolfwolfgod.mode");
		if("redis".equalsIgnoreCase(mode)) {
			source = Source.fromGraph(new SubscriberSource(deviceId)).map(x->buildFromJsonString(x));			
		}else {
	        source = Source.unfold(thingEventGenerator.seed(), (Function<ThingEvent, Optional<Pair<ThingEvent, ThingEvent>>>) last -> {
        	ThingEvent next = thingEventGenerator.newEvent(last);
            return Optional.of(Pair.apply(next, next));
	        });
			
		}
	}
	
	private ThingEvent buildFromJsonString(Message m) {
		JsonNode node = Json.parse(m.getContext());
		String deviceId = node.findPath("deviceId").asText();
        String property = node.findPath("property").asText();
        String value = node.findPath("value").asText();
		return new ThingEvent(deviceId,property,value);
	}
	public Source<ThingUpdate, NotUsed> update(){
		return source.throttle(1, duration, 1, ThrottleMode.shaping())
				.map(tq->new ThingUpdate(tq.deviceId, tq.property, tq.value));
	}
	
    @Override
    public String toString() {
        return "Thing(" + deviceId + ")";
    }
}
