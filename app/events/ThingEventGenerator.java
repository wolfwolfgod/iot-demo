package events;

public interface ThingEventGenerator {
	ThingEvent newEvent(ThingEvent last);

	ThingEvent seed();

}
