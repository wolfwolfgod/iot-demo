package events;

public class ThingEvent {
	public final String deviceId;
	public final String property;
	public final String value;
	
	
	public ThingEvent(String deviceId, String property, String value) {
		this.deviceId = deviceId;
		this.property = property;
		this.value = value;
	}
	
}
