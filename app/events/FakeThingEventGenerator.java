package events;

public class FakeThingEventGenerator implements ThingEventGenerator {

	private final String deviceId;
	private String lastStatus="close";
	

	public FakeThingEventGenerator(String deviceId) {
		super();
		this.deviceId = deviceId;
	}

	@Override
	public ThingEvent newEvent(ThingEvent last) {
		int i= ((int)(Math.random()*100)) %2;
		switch (i) {
		case 0:
			ThingEvent event = new ThingEvent(last.deviceId, "status", lastStatus.equalsIgnoreCase("open")?"close":"open");
			lastStatus = event.value;
			return event;
		default:
			return new ThingEvent(last.deviceId, "temperature", String.valueOf((int)(Math.random()*100)));
		}
	}


	@Override
	public ThingEvent seed() {
		return new ThingEvent(deviceId, "status", "open");
	}

}
