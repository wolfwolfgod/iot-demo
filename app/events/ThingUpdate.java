package events;

public class ThingUpdate {
	public final String deviceId;
	public final String property;
	public final String value;
	
	
	public ThingUpdate(String deviceId, String property, String value) {
		super();
		this.deviceId = deviceId;
		this.property = property;
		this.value = value;
	}
	
	public String getDeviceId() {
		return deviceId;
	}
	public String getProperty() {
		return property;
	}
	public String getValue() {
		return value;
	}
	


}
