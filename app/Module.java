import com.google.inject.AbstractModule;
import play.libs.akka.AkkaGuiceSupport;
import actors.*;

@SuppressWarnings("unused")
public class Module extends AbstractModule implements AkkaGuiceSupport {

	@Override
	protected void configure() {
//		bindActor(ThingsActor.class, "thingsActor");
		bindActor(ClientParentActor.class, "clientParentActor");
		bindActorFactory(ClientActor.class, ClientActor.Factory.class);
	}

}
