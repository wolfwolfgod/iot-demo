package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import play.libs.Json;
import play.shaded.ahc.org.asynchttpclient.AsyncHttpClient;
import play.shaded.ahc.org.asynchttpclient.AsyncHttpClientConfig;
import play.shaded.ahc.org.asynchttpclient.DefaultAsyncHttpClient;
import play.shaded.ahc.org.asynchttpclient.DefaultAsyncHttpClientConfig;
import play.shaded.ahc.org.asynchttpclient.ws.WebSocket;

import org.awaitility.Duration;
import org.junit.Test;
import play.test.TestServer;

import java.util.Collections;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.fail;
import static play.test.Helpers.running;
import static play.test.Helpers.testServer;
import static org.awaitility.Awaitility.*;

public class FunctionalTest {

    @Test
    public void testListenStatus() {
        TestServer server = testServer(19001);
        running(server, () -> {
            try {
                AsyncHttpClientConfig config = new DefaultAsyncHttpClientConfig.Builder().setMaxRequestRetry(0).build();
                AsyncHttpClient client = new DefaultAsyncHttpClient(config);
                WebSocketClient webSocketClient = new WebSocketClient(client);

                try {
                    String serverURL = "ws://localhost:19001/ws";
                    ArrayBlockingQueue<String> queue = new ArrayBlockingQueue<String>(10);
                    WebSocketClient.LoggingListener listener = new WebSocketClient.LoggingListener((message) -> {
                        try {
                            queue.put(message);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    });
                    CompletableFuture<WebSocket> completionStage = webSocketClient.call(serverURL, listener);

                    await().until(completionStage::isDone);
                    WebSocket websocket = completionStage.get();
                    await().until(() -> websocket.isOpen());
                    // add listen to device property status when it is open
                    websocket.sendMessage(" {\"deviceId\":\"7784733de\",\"command\":\"watch\",\"property\":\"status\",\"operation\":\"EQ\",\"targetValue\":\"open\"}");
                    await().until(() -> queue.peek() != null);
                    String input = queue.take();

                    JsonNode json = Json.parse(input);
                    String property = json.get("property").asText();
                    String value = json.get("value").asText();
                    
                    // receive message should be open status
                    assertThat(property.equals("status") && value.equals("open")).isTrue();
                    
                    // add another listen to device property status when it is close
                    websocket.sendMessage(" {\"deviceId\":\"7784733de\",\"command\":\"watch\",\"property\":\"status\",\"operation\":\"EQ\",\"targetValue\":\"close\"}");
                    await().until(() -> queue.peek() != null);
                    input = queue.take();

                    json = Json.parse(input);
                    property = json.get("property").asText();
                    value = json.get("value").asText();
                    
                    // now receive message should be open or close status
                    assertThat(property.equals("status")).isTrue();
                    assertThat(Collections.singletonList(value)).isSubsetOf("open","close");
                    
                    //remove listen on status
                    websocket.sendMessage("{\"deviceId\":\"7784733de\",\"command\":\"unwatch\",\"property\":\"status\"}");
                    
                  //wait 10 seconds, and then should no message exist in queue.
                    await().atLeast(new Duration(10, TimeUnit.SECONDS));
                    assertThat(queue.peek() == null).isTrue();
                    
                    
                } finally {
                    client.close();
                }
            } catch (Exception e) {
                fail("Unexpected exception", e);
            }
        });
    }
    
    @Test
    public void testListenTemerature() {
        TestServer server = testServer(19001);
        running(server, () -> {
            try {
                AsyncHttpClientConfig config = new DefaultAsyncHttpClientConfig.Builder().setMaxRequestRetry(0).build();
                AsyncHttpClient client = new DefaultAsyncHttpClient(config);
                WebSocketClient webSocketClient = new WebSocketClient(client);

                try {
                    String serverURL = "ws://localhost:19001/ws";
                    ArrayBlockingQueue<String> queue = new ArrayBlockingQueue<String>(10);
                    WebSocketClient.LoggingListener listener = new WebSocketClient.LoggingListener((message) -> {
                        try {
                            queue.put(message);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    });
                    CompletableFuture<WebSocket> completionStage = webSocketClient.call(serverURL, listener);

                    await().until(completionStage::isDone);
                    WebSocket websocket = completionStage.get();
                    await().until(() -> websocket.isOpen());
                    // add listen to device property temperature when it is greater then 3
                    websocket.sendMessage("{\"deviceId\":\"7784733de\",\"command\":\"watch\",\"property\":\"temperature\",\"operation\":\"GT\",\"targetValue\":\"3\"}");
                    await().until(() -> queue.peek() != null);
                    String input = queue.take();

                    JsonNode json = Json.parse(input);
                    String property = json.get("property").asText();
                    int value = json.get("value").asInt();
                    
                    // receive message should be open status
                    assertThat(property.equals("temperature") && value>3).isTrue();
                    
                    // add another listen to device property temperature when it is less than 90
                    websocket.sendMessage("{\"deviceId\":\"7784733de\",\"command\":\"watch\",\"property\":\"temperature\",\"operation\":\"LT\",\"targetValue\":\"90\"}");
                    await().until(() -> queue.peek() != null);
                    input = queue.take();

                    json = Json.parse(input);
                    property = json.get("property").asText();
                    value = json.get("value").asInt();
                    
                    // now receive message should be open or close status
                    assertThat(property.equals("temperature") && value<90).isTrue();
 
                    //remove listen on temperature
                    websocket.sendMessage("{\"deviceId\":\"7784733de\",\"command\":\"unwatch\",\"property\":\"temperature\"}");

                    //wait 10 seconds, and then should no message exist in queue.
                    await().atLeast(new Duration(10, TimeUnit.SECONDS));
                    assertThat(queue.peek() == null).isTrue();

                } finally {
                    client.close();
                }
            } catch (Exception e) {
                fail("Unexpected exception", e);
            }
        });
    }    
}
